<html>
<body>
<?php

//open daftar.php
if (isset($_GET["btndftr"])){
	$name = $_GET["txtname"]; // diambil dari name di form
	$NIM = $_GET["txtNIM"]; // diambil dari name di form
	$Username = $_GET["txtUsername"]; // diambil dari name di form
	$Password= $_GET["txtPassword"]; // diambil dari name di form
	$UlangPassword= $_GET["txtUlangPassword"]; // diambil dari name di form
}else{
	die("Anda harus mengisi yang benar lewat form pendaftaran");
}

//masukan nama
if (!empty($name)){
	if (!preg_match("/^[a-zA-Z\s]*$/",$name)) {
      echo "<b>Nama hanya boleh huruf!</b><br>";
    }else{
		echo "Thanks, <b>". $name."</b><br>";
	}
}else{
	echo("Anda belum memasukkan nama <br>");
}

//masukan NIM
if (!empty($NIM)){
	if (!preg_match("/^[0-9]*$/",$NIM)) {
      echo "<b>NIM hanya boleh terdiri dari angka</b><br>"; 
    }else{
		echo "NIM, <b>". $NIM."</b><br>";
	}
}else{
	echo("Anda belum memasukkan NIM <br>");
}

//masukan Username
if (!empty($Username)){
	$lUsername = strlen($Username);
	if (!preg_match("/^[a-zA-Z\s]*$/",$Username)) {
		echo ("<b> Username hanya boleh terdiri dari huruf</b><br>") ; 
    }elseif ((($lUsername <= 6)) || (($lUsername >= 15))){
		echo ("<b> Username minimal 6 karakter dan maksimal 15 karakter</b> <br>");
	}else{
			echo "Username anda: <b>". $Username."</b><br>";
		}
}else{
	echo("Anda belum memasukkan Username <br>");
}

//masukan Password
if (!empty($Password)){
	if (!preg_match("/^[0-9]*$/",$Password)) {
      echo "<b>Password hanya boleh terdiri dari angka</b><br>"; 
    }elseif ($Password != $UlangPassword){
		echo "Password satu dan dua tidak sama";
	}else{
		echo "Password anda : <b>". $Password."</b><br>";
	}
}else{
	echo("Anda belum memasukkan Password <br>");
}
?>
</body>
</html>