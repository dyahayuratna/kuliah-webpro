<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <style>
            tr td{
                padding: 5px;
            }
            .judul{
                background-color: #44ac44;
                color: white;
                font-weight: bold;
            }
            .isi{
                background-color: #eaeaea;
            }
            input[type="submit"]{
                background-color: #44ac44;
                border: none;
                color: white;
                padding: 5px;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <form action="jurnal9-kalkulator-hitung.php" method="POST">
            <table>
                <tr class="judul">
                    <td colspan="2" align="center">FORM INPUTAN NILAI</td>
                </tr>
                <tr class="isi">
                    <td>Nilai TP</td>
                    <td><input type="text" name="nilai_tp" required></td>
                </tr>
                <tr>
                    <td>Nilai Tes Awal</td>
                    <td><input type="text" name="nilai_tes_awal" required></td>
                </tr>
                <tr  class="isi">
                    <td>Nilai Jurnal</td>
                    <td><input type="text" name="nilai_jurnal" required></td>
                </tr>
                <tr>
                    <td>Nilai Tes Akhir</td>
                    <td><input type="text" name="nilai_tes_akhir" required></td>
                </tr>
                <tr class="isi">
                    <td colspan="2" align="right"><input type="submit"></td>
                </tr>
            </table>
        </form>
    </body>
</html>
