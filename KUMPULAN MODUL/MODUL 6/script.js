function validateForm() {
  let x = document.forms["myForm"]["fnumb"].value;
  if (x == "") {
    alert("Nomor Handphone tidak boleh kosong");
    return false;
  }

  if (isNaN(x)) {
    alert("Nomor Handphone harus angka");
    return false;
  }
  
  if (x.length != 12) {
    alert("Nomor Handphone harus 12 digit");
    return false;
  }
}
