<?php
include ('../lib/dbconnection.php');

$id = $_GET['user_id']??null;

if(isset($id)){
    $sql = "SELECT * FROM posts WHERE user_id = $id";
} else {
    $sql = "SELECT * FROM posts";
}

$result = $conn->query($sql);
$posts = [];
while($row = $result->fetch_assoc()){
    $posts[] = $row;
}

echo json_encode($posts);

?>