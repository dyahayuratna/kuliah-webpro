<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modul 7 - PHP</title>
    <link rel="stylesheet" href="mod 7.css"/>
</head>
<body>

    <h1>
    <?php
    echo "Kuliah Web Programming 45-02";
    ?>
    </h1>

    <?php
    $name     = "DYAH AYU RATNA NINGSIH";
    $nim      = "6702213009";
    $email    = "dyah020502@gmail.com";
    $noHP     = "0895332858801";
    $noWA     = "6282112458786"; //Wajib pakai kode negara (kode Indonesia = 62)
    $tglLahir = "2002-05-02";
    $umur     = hitungUmur($tglLahir);

    function hitungUmur($birthdayDate){
        $date = new DateTime($birthdayDate);
        $now = new DateTime();
        $interval = $now->diff($date);
        return $interval->y;
    }

    echo "Nama Saya     : ";
    echo $name;
    echo "<br>";
    echo "NIM Saya      : ";
    echo $nim;
    echo "<br>";
    echo "Email Saya    : ";
    echo " <a href='mailto://".$email."' target='_blank'>".$email."</a>";
    echo "<br>";
    echo "Ho HP Saya    : ";
    echo " <a href='tel://".$noHP."' target='_blank'>".$noHP."</a>";
    echo "<br>";
    echo "Ho WA Saya    : ";
    echo " <a href='https://wa.me/".$noWA."' target='_blank'>".$noWA."</a>";
    echo "<br>";
    echo "Tgl Lahir Saya : ";
    echo $tglLahir;
    echo "<br>";
    echo "Umur Saya     : ";
    echo $umur." Tahun";
    echo "<br>";
    echo "2 Tahun yang lalu saya berumur ".($umur-2)." Tahun<br>";
    echo "Tahun depan saya akan berumur ".($umur+1)." Tahun<br>";
    echo "</br>";

    echo "<a href='perhitungan.php'> Klik untuk belajar perhitungan dengan PHP </a";
    echo "<br></br>";
    echo "<a href='lingkaran.php'> Klik Tugas Menghitung Luas Lingkaran Dengan PHP </a";
    echo "<br></br>";
    echo "<a href='percabangan.php'> Klik Tugas Percabangan Switch Dengan PHP </a";
    echo "<br></br>";
    echo "<a href='perulangan.php'> Klik Tugas Perulangan For dan Do While Dengan PHP </a";

?>

</body>
</html>
