<title>Percabangan Switch Nama Bulan</title>
<link rel="stylesheet" href="mod 7.css"/>

<?php

// Percabangan Switch
echo "<h2> Percabangan Switch </h2>";
echo "<h5> DYAH AYU RATNA NINGSIH_6702213009 </h5>";
$Bulan = 11 ; // Pemanggilan untuk nama bulan yang ingin ditampilkan

switch ($Bulan){ // Percabangan dengan nama-nama bulan dalam satu tahun

    case 1 : // Percabangan pertama
        echo "Sekarang adalah bulan Januari"; // keluaran bulan pertama yaitu Januari
        break;
    case 2 : // Percabangan kedua
        echo "Sekarang adalah bulan Februari"; // keluaran bulan kedua yaitu Februari
        break;
    case 3 : // Percabangan ketiga
        echo "Sekarang adalah bulan Maret"; // keluaran bulan ketiga yaitu Maret
        break;
    case 4 : // Percabangan keempat
        echo "Sekarang adalah bulan April"; // keluaran bulan keempat yaitu April
        break;
    case 5 : // Percabangan kelima
        echo "Sekarang adalah bulan Mei"; // keluaran bulan kelima yaitu Mei
        break;
    case 6 : // Percabangan keenam
        echo "Sekarang adalah bulan Juni"; // keluaran bulan keenam yaitu Juni
        break;
    case 7 : // Percabangan ketujuh
        echo "Sekarang adalah bulan Juli"; // keluaran bulan ketujuh yaitu Juli
        break;
    case 8 : // Percabangan kedelapan
        echo "Sekarang adalah bulan Agustus"; // keluaran bulan kedelapan yaitu Agustus
        break;
    case 9 : // Percabangan kesembilan
        echo "Sekarang adalah bulan September"; // keluaran bulan kesembilan yaitu September
        break;
    case 10 : // Percabangan kesepuluh
        echo "Sekarang adalah bulan Oktober"; // keluaran bulan kesepuluh yaitu Oktober
        break;
    case 11 : // Percabangan kesebelas
        echo "Sekarang adalah bulan November"; // keluaran bulan kesebelas yaitu November
        break;
    case 12 : // Percabangan keduabelas
        echo "Sekarang adalah bulan Desember"; // keluaran bulan keduabelas yaitu Desember
        break;
    default: // Kondisi gagal atau salah
        echo " Pencarian tidak ditemukan "; // apabila yang diinputkan tidak sesuai percabangan yang telah ditentukan maka akan mengeluarkan data tidak ditemukan
}

?>
