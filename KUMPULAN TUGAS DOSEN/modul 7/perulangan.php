<title>Perulangan Dengan For dan Do While</title>
<link rel="stylesheet" href="mod 7.css"/>

<?php

// Perulangan Dengan For dan Do While
echo "<h2> Perulangan Dengan For dan Do While </h2>";
echo "<h5> DYAH AYU RATNA NINGSIH_6702213009 </h5>";

// Perulangan dengan for
echo "<b> === Perulangan Dengan For === </b>";
echo "<br></br>";
// Tipe data untuk x = 10
$x = 10;
for ($x = 10; $x > 0; $x--){ // perulangan mundur dari 10 sampai dengan 1
    echo "Perulangan ke- "; // keluaran dengan tulisan Perulangan ke-
    echo $x; // keluaran angka yang melakukan perulangan
    echo "<br>"; // space
}

// Perulangan dengan for bersarang
echo "<br>";
echo "<b> === Perulangan Dengan For Bersarang === </b>";
echo "<br></br>";
// Tipe data untuk x = 10; y =5
$x = 10;
$y = 5;
for ($x = 10; $x > 0; $x--){  // perulangan mundur atau decrement dari 10 sampai dengan 1
    for ($y = 5; $y < $x; $y++ ){ // perulangan mundur yang tiap mundurnya dimulai sebanyak 5x, 4x, 3x, 2x, lalu 1x
        echo "Perulangan ke- "; // keluaran dengan tulisan Perulangan ke-
        echo $x; // keluaran angka yang melakukan perulangan
        echo "<br>"; // space
    }  
}

// Perulangan dengan do while
echo "<br>";
echo "<b> === Perulangan Dengan Do While=== </b>";
echo "<br></br>";
// Tipe data untuk x = 5
$x = 5;
do {
    
    echo "Perulangan ke- "; // keluaran dengan tulisan Perulangan ke-
    echo $x; // keluaran angka yang melakukan perulangan
    echo "<br>"; // space
    $x++; // increment
} while ($x <= 10) // 5 <= 10 ma a angka 10 diikutsertakan
?>