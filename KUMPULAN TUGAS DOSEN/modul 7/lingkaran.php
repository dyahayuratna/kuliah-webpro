    <title>Menghitung Luas Lingkaran<</title>
    <link rel="stylesheet" href="mod 7.css"/>

<?php

//Menghitung Luas Lingkaran
echo "<h2> Menghitung Luas Lingkaran </h2>";
echo "<h5> DYAH AYU RATNA NINGSIH_6702213009 </h5>";

// Tipe data untuk jari-jari dan phi
$JariJari = 8;
$Phi  = 22/7;

// Keluaran yang ditampilkan jari-jari dan phi
echo "Phi = ".$Phi." ";
echo "<br>";
echo "Jari-Jari = ".$JariJari." cm";
echo "<br>";

// Rumus Luas Lingkaran {22/7*(jari-jari*jari-jari)} dengan satuan cm
echo "Luas Lingkaran = ".$Phi*($JariJari * $JariJari)." cm";
echo "<br>";
echo "<br>";

?>
