<link rel="stylesheet" href="mod 7.css"/>
<?php

//Operasi Aritmatika
$A = 5;
$B = 4;
echo "Operasi Aritmatika<br>";
echo "A+B= ".($A+$B);
echo "<br>";
echo "A-B= ".($A-$B);
echo "<br>";
echo "AxB= ".($A*$B);
echo "<br>";
echo "A:B= ".($A/$B);
echo "<br>";
echo "<br>";

//Menghitung Luas Persegi
$panjang = 6;
$lebar = 12;
echo "Menghitung Luas Persegi<br>";
echo "panjang= ".$panjang."m";
echo "<br>";
echo "lebar= ".$lebar."m";
echo "<br>";
echo "Luas Persegi= ".($panjang * $lebar)."m<sup>2</sup>";
echo "<br>";
echo "<br>";

//Menghitung Segitiga
$alas = 5;
$tinggi = 10;
echo "Menghitung Luas Segitiga<br>";
echo "alas= ".$alas."m";
echo "<br>";
echo "tinggi= ".$tinggi."m";
echo "<br>";
echo "Luas Segitiga= ".(0.5*$alas * $tinggi)."m<sup>2</sup>";
echo "<br>";
echo "<br>";

//IF ELSE
echo "Ganjil Genap<br>";
$bilangan = 5;
if($bilangan%2 == 0){
    echo $bilangan." adalah bilangan GENAP";
}
else{
    echo $bilangan." adalah bilangan GANJIL";
}
echo "<br>";
echo "<br>";

//Looping
$ayam = 5;
echo "Ayo Bernyayi<br>";
while($ayam>0){
    echo "Anak ayam turunlah ".$ayam.", mati satu tinggalah ".($ayam-1)."<br>";
    $ayam--;
}
?>
