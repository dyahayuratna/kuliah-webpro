<html>
<body>
<?php
//open daftar.php
if (isset($_POST["btndftr"])){
	$name = $_POST["txtname"]; // diambil dari name di form
	$nohp = $_POST["txtnohp"]; // diambil dari name di form
	$tempatlahir= $_POST["txttmptlahir"]; // diambil dari name di form
}else{
	die("Anda harus mengisi yang benar lewat form pendaftaran");
}
//masukan nama
if (!empty($name)){
	if (!preg_match("/^[a-zA-Z\s]*$/",$name)) {
      echo "<b>Nama hanya boleh huruf!</b><br>";
    }else{
		echo "Thanks, <b>". $name."</b><br>";
	}
}else{
	echo("Anda belum memasukkan nama <br>");
}

//masukan no hp
if (!empty($nohp)){
	if (!preg_match("/^[0-9]*$/",$nohp)) {
      echo "<b>No HP hanya boleh terdiri dari angka</b><br>"; 
    }else{
		echo "No Hp anda, <b>". $nohp."</b><br>";
	}
}else{
	echo("Anda belum memasukkan no hp <br>");
}

//tempat lahir
if (!empty($tempatlahir)){
	$ltempatlahir = strlen($tempatlahir);
	if (!preg_match("/^[a-zA-Z\s]*$/",$tempatlahir)) {
		echo ("<b>Tempat Lahir hanya boleh terdiri dari huruf</b><br>") ; 
    }elseif ((($ltempatlahir <= 3)) || (($ltempatlahir >= 15))){
		echo ("<b>Tempat lahir minimal 3 karakter dan maksimal 15 karakter</b> <br>");
	}else{
			echo "Tempat lahir anda: <b>". $tempatlahir."</b><br>";
		}
}else{
	echo("Anda belum memasukkan tempat lahir <br>");
}
?>
</body>
</html>
