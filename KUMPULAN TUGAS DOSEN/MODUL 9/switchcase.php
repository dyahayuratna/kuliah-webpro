<?php
$warna = "ungu";

switch ($warna) {
    case 'merah':
        echo '<p style="color:red"> Ini warna tosca </p>';
        break;
    case 'ungu':
        echo '<p style="color:violet"> Ini warna ungu </p>';
        break;
    case 'hijau':
        echo '<p style="color:green"> Ini warna hijau </p>';
        break;
    default:
        echo '<p style="color:blue"> Ini warna biru </p>';
        break;
}
?>